package vn.com.fsoft.mtservice.bean.interceptor;

import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.constant.enumeration.PermissionEnum;

/**
 * 
 * @author hungxoan
 *
 */
public interface Interceptor {
    Boolean isAcknowledged(IncomingRequestContext context);
    Boolean isAcknowledged(IncomingRequestContext context, PermissionEnum permissionEnum);
}
