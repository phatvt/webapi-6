package vn.com.fsoft.mtservice.bean.interceptor;


import vn.com.fsoft.mtservice.bean.data.UserRepo;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.constant.enumeration.PermissionEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author hungxoan
 *
 */

@Component("roleCheckingInterceptor")
public class RoleCheckingInterceptor implements Interceptor {

    @Autowired
    private UserRepo userRepo;

    @Override
    public Boolean isAcknowledged(IncomingRequestContext context) {
        return null;
    }

    @Override
    public Boolean isAcknowledged(IncomingRequestContext context, PermissionEnum permissionEnum) {

        if(context == null) {
            return Boolean.FALSE;
        }

        Integer userId = context.getUserId();

        if(userId == null || userId == 0) {
            return Boolean.FALSE;
        }

        RepoStatus<Boolean> status = userRepo.isUserOwnRole(permissionEnum, userId);

        if(status.getObject() == null) {
            return Boolean.FALSE;
        }

        return status.getObject();
    }
}
