package vn.com.fsoft.mtservice.object.base;

import vn.com.fsoft.mtservice.object.entities.HibernateRootEntity;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
public class CommandResponseStatus<T extends HibernateRootEntity> extends Status {

    private Integer index;

    public CommandResponseStatus() {
        super();
    }

    public CommandResponseStatus(String code, String message) {
        super(code, message);
    }

    public CommandResponseStatus(String code, String message, Integer index) {
        super(code, message);
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

}
