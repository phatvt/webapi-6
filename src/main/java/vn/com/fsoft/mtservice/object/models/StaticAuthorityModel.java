package vn.com.fsoft.mtservice.object.models;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */
public class StaticAuthorityModel {

    private Integer id;
    private String code;
    private String name;
    private String masterCategory;
    private List<PermissionModel> permissions;

    public StaticAuthorityModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMasterCategory() {
        return masterCategory;
    }

    public void setMasterCategory(String masterCategory) {
        this.masterCategory = masterCategory;
    }

    public List<PermissionModel> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionModel> permissions) {
        this.permissions = permissions;
    }
}
