package vn.com.fsoft.mtservice.object.models;

import vn.com.fsoft.mtservice.object.base.ResultTransformer;

/**
 * 
 * @author hungxoan
 *
 */

public class RoleIDModel extends ResultTransformer {

    private Integer id;

    public RoleIDModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
