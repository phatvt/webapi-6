package vn.com.fsoft.mtservice.object.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

/**
 * 
 * @author hungxoan
 *
 */
@Entity(name = "UserEntity")
@Table(name = "users", schema = DatabaseConstants.SCHEMA)
public class UserEntity extends TrackingFieldEntity implements Serializable {
	
	@Id
    @Column(name = "id")
	@GeneratedValue(generator = "users_value_generator",
    strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "users_value_generator", schema = DatabaseConstants.SCHEMA,
			sequenceName = "users_seq")
    private Integer id;
	
	@Column(name = "user_name")
	private String userName; 
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "user_secret")
	private String secret;

	@Column(name = "first_login")
	private Boolean firstLogin;

	@Column(name = "used")
	private Boolean used;

	@OneToOne(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
			fetch = FetchType.LAZY)
	@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	private ProfileEntity profile;

	public UserEntity() {
		super();
	}

	@JsonView(Views.Public.class)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonView(Views.Public.class)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonView(Views.Public.class)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonView(Views.Hidden.class)
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Boolean getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(Boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	@JsonView(Views.Public.class)
    public ProfileEntity getProfile() {
        return profile;
    }

    public void setProfile(ProfileEntity profile) {
		profile.setUserId(this);
        this.profile = profile;
    }

	@JsonView(Views.Hidden.class)
	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}
}
