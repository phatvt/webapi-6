package vn.com.fsoft.mtservice.object.entities;

import java.io.Serializable;

/**
 * 
 * @author hungxoan
 *
 */
public class HibernateRootEntity extends Object implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8796866730186291292L;
}
